//MODEL
let data
let table

function getJSON(){
    let url = document.getElementById('editableContainer').getAttribute('data-source')
    fetch(url)
        .then(
            rep => rep.json()
        )
        .then(
            json_data => {
                data = json_data
                displayHTML()
            }
        )
        .catch(e => console.log(e));
}

function updateData(obj_index, key, valeur){
    data[obj_index][key] = valeur
}

//VIEW
function displayHTML(){
    table = document.createElement('table')
    let container = document.getElementById('editableContainer')
    container.appendChild(table)
    displayHead()
    displayTable()
}

function displayHead() {
    let header = document.createElement('thead')
    let tr = document.createElement('tr')
    let obj = data[0]
    for (const objKey in obj) {
        let th = document.createElement('th')
        th.innerText = objKey
        th.addEventListener("click", trier)
        tr.appendChild(th)
    }
    header.appendChild(tr)
    table.appendChild(header)
}

function displayTable(){
    let tbody = document.createElement('tbody')
    let index = 0
    for (const obj of data) {
        let tr = document.createElement('tr')
        tr.classList.add("ligne")
        for (const key in obj) {
            let td = createCell(obj, key, index)
            tr.appendChild(td)
        }
        tbody.appendChild(tr)
        index++
    }
    let oldBody = table.getElementsByTagName('tbody')[0]
    if (oldBody) oldBody.remove()
    table.appendChild(tbody)
}

function createCell(obj, key, index){
    let td = document.createElement('td')
    td.innerText = obj[key]
    td.contentEditable = "true"
    td.setAttribute('data-key', key)
    td.setAttribute('data-index', index)
    td.addEventListener('input', modifCell)
    return td
}

//CONTROL
function filtrer(e){
    let search = e.target.value
    let lignes = document.querySelectorAll('.ligne')
    for (const ligne of lignes) {
        if(ligne.innerText.includes(search)){
            ligne.style.display = 'table-row'
        }else{
            ligne.style.display = 'none'
        }
    }
}
document.getElementById('inputFiltre')
    .addEventListener('input', filtrer)

function trier(e){
    let key = e.target.textContent
    data.sort(
        (a,b) => a[key].toString().localeCompare(b[key].toString())
    )
    displayTable()
}

function ajouter(e){
    let newObj = {}
    for (const key in data[0]) {
        newObj[key] = ""
    }
    data.push(newObj)
    displayTable()
}
document.getElementById('btnAdd').addEventListener('click', ajouter)

function modifCell(e){
    let val = e.target.innerText
    let key = e.target.getAttribute('data-key')
    let index = e.target.getAttribute('data-index')
    updateData(index, key, val)
}

function save(e){
    let jsonData = JSON.stringify(data)
    fetch('up.php',
        {
            method: 'POST',
            headers: {
                "content-type": "application/json"
            },
            body: jsonData
        })
        .then(
            r => r.json()
        )
        .then(
            d => console.log(d)
        )
}
document.getElementById('btnSav').addEventListener('click', save)

getJSON()